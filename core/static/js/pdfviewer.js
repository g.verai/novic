var pdfviewer=(function(){
    let pdf ;
    let canvas;
    let isPageRendering;
    let  pageRenderingQueue = null;
    let canvasContext;
    let totalPages;
    let currentPageNum = 1;
    
    // events
    window.addEventListener('load', function () {
        isPageRendering= false;
        pageRenderingQueue = null;
        canvas = document.getElementById('pdf_canvas');
        canvasContext = canvas.getContext('2d');
        
        initEvents();
        initPDFRenderer(canvas.getAttribute('data-pdfpath'));
    });
    
    function initEvents() {
        
        let pselect = document.querySelectorAll('.pagesel');
        pselect.forEach(psel=>{
            psel.addEventListener('change',selchangepag);
        });
        document.querySelectorAll('.changecap').forEach(c=>c.addEventListener('change',changecap));
        document.querySelectorAll('.pagenext').forEach(c=>c.addEventListener('click',renderNextPage));
        document.querySelectorAll('.pageprev').forEach(c=>c.addEventListener('click',renderPreviousPage));
        document.querySelectorAll('.pagefirst').forEach(c=>c.addEventListener('click',renderFirstPage));
        document.querySelectorAll('.pagelast').forEach(c=>c.addEventListener('click',renderLastPage));
        
        document.querySelectorAll('.cap-prev').forEach(c=>c.addEventListener('click',nextchapter));
        document.querySelectorAll('.cap-next').forEach(c=>c.addEventListener('click',prevchapter));
    }
    function selchangepag(evt){
        goToPageNum(evt)
    }
    function changecap(evt){
        window.location.href='../'+evt.target.value+'/';
    }
    // init when window is loaded
    function initPDFRenderer(url) {
       
        let option  = { url};
        
    
        pdfjsLib.getDocument(option).promise.then(pdfData => {
            totalPages = pdfData.numPages;
            let pselect = document.querySelectorAll('.pagesel');
            pselect.forEach(psel=>{
                while(psel.childNodes.length>0)psel.removeChild(psel.lastChild);
                for(let i=1;i<=totalPages;i++){
                    let op = psel.appendChild(document.createElement('option'));
                    op.value=i;
                    op.textContent=i;
                }
            })

           
            pdf = pdfData;
            console.log(pdfData);
            renderPage(currentPageNum);
        });
    }
    
    function renderPage(pageNumToRender = 1, scale = 1) {
        isPageRendering = true;
        
        let pselect = document.querySelectorAll('.pagesel');
            pselect.forEach(psel=>{
                psel.value=pageNumToRender;
            })
        pdf.getPage(pageNumToRender).then(page => {
            const viewport = page.getViewport({scale :3});
            canvas.height = viewport.height;
            canvas.width = viewport.width;  
            let renderCtx = {canvasContext ,viewport};
            page.render(renderCtx).promise.then(()=> {
                isPageRendering = false;
                if(pageRenderingQueue !== null) { // this is to check of there is next page to be rendered in the queue
                    renderPage(pageNumToRender);
                    pageRenderingQueue = null; 
                }
            });        
        }); 
    }
    
    function renderPageQueue(pageNum) {
        if(pageRenderingQueue != null) {
            pageRenderingQueue = pageNum;
        } else {
            renderPage(pageNum);
        }
    }
    
    function renderNextPage(ev) {
        if(currentPageNum >= totalPages) {
            //alert("This is the last page");
            return ;
        } 
        currentPageNum++;
        renderPageQueue(currentPageNum);
    }
    
    function renderPreviousPage(ev) {
        if(currentPageNum<=1) {
            //alert("This is the first page");
            return ;
        }
        currentPageNum--;
        renderPageQueue(currentPageNum);
    }
    function renderFirstPage(evt){
        currentPageNum=1;
        renderPageQueue(currentPageNum);
    }
    function renderLastPage(evt){
        currentPageNum=totalPages;
        renderPageQueue(currentPageNum);
    }
    function goToPageNum(evt) {
        let numberInput =evt.target// document.getElementById('page_num');
        let pageNumber = parseInt(numberInput.value);
        if(pageNumber) {
            if(pageNumber <= totalPages && pageNumber >= 1){
                currentPageNum = pageNumber;
                numberInput.value ="";
                renderPageQueue(pageNumber);
                return ;
            }
        }
            
    }
    function nextchapter(evt){
        let selcap = document.querySelector('.changecap');
        let selidx = selcap.selectedIndex;
        if(selidx<=0)return;
        selidx--;
        let opt = selcap.options[selidx];
        window.location.href='../'+opt.value+'/';
    }
    function prevchapter(evt){
        let selcap = document.querySelector('.changecap');
        let selidx = selcap.selectedIndex;
        if(selidx>=selcap.options.length-1)return;
        selidx++;
        let opt = selcap.options[selidx];
        window.location.href='../'+opt.value+'/';
    }
})();
