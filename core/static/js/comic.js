$(document).ready(function () {
    $('.navmain').removeClass('fixed-top')
    $('.navmain').addClass('navbar-dark');
    $('.add-comment').on('click', _addcomment);
    $('.del-comm-ok').on('click', _deletecomment);
    $(document).on('click', '.add-reply', _addreply);
    $(document).on('click', '.del-comic-ok', _deletecomic);
    $(document).on('change', '.cap-pdf', _cappdf_change);
    $(document).on('submit', '.cap-modif', _modcap);
    $(document).on('shown.bs.modal', '.mod-cap', _modpdf_shown);
    $(document).on('click','.favorite',_togglefav);
    $(document).on('click','.bookmark',_togglebm);
    
});
function _addcomment(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    var btn = $(this);
    var txt = $('.txt-comment').val();
    var cid = btn.attr('data-cid');

    if (!txt) return;
    var _d = {
        cid: cid
        , txt: txt
    }
    var csrftoken = Cookies.get('csrftoken');
    $.ajax({
        url: '/agregar_comentario_ajax/',
        beforeSend: function (request) {
            request.setRequestHeader("X-CSRFToken", csrftoken);
        },
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(_d),
        dataType: 'text',
        success: function (result) {
            var res = JSON.parse(result);
            var ccont = $('.comment-container');
            var ccard = ccont.find('.card').first();
            if (!ccard || !ccard.length) {
                ccont.append(res.comment);
            } else {
                $(res.comment).insertBefore(ccard);//una mierda jquery
            }
            $('.txt-comment').val('');
            //$('.add-comment').on('click', _addcomment);
            //nc.find('.del-comm-ok').on('click', _deletecomment);
            //nc.find('.add-reply').on('click', _addreply)
        }
    });
}
function _deletecomment(evt) {
    var btn = $(this)
    var commid = btn.attr('data-commid');
    var cont = btn.closest('.card')
    var _d = {
        commid: commid
    }
    var csrftoken = Cookies.get('csrftoken');
    $.ajax({
        url: '/eliminar_comentario_ajax/',
        beforeSend: function (request) {
            request.setRequestHeader("X-CSRFToken", csrftoken);
        },
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(_d),
        dataType: 'text',
        success: function (result) {

            $('#del-confirm-' + commid).modal('hide');
            var res = JSON.parse(result);
            if (res.result == 'ok') setTimeout(function () { cont.remove(); }, 500)

        }
    });
}
function _addreply(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    var btn = $(this);
    var tar = btn.closest('.form-group').find('.txt-comment-reply');
    var txt = tar.val();
    if (!txt) return;
    var cid = btn.attr('data-cid');
    var commid = btn.attr('data-commentid');
    var cont = btn.closest('.row')
    var _d = {
        commid: commid
        , cid: cid
        , txt: txt
    }
    //
    var csrftoken = Cookies.get('csrftoken');
    $.ajax({
        url: '/agregar_comentario_resp_ajax/',
        beforeSend: function (request) {
            request.setRequestHeader("X-CSRFToken", csrftoken);
        },
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify(_d),
        dataType: 'text',
        success: function (result) {

            var res = JSON.parse(result);

            var next = cont.next();
            if (next.length)
                $(res.comment).insertBefore(cont.next());
            else $(res.comment).insertAfter(cont);
            tar.val('');
            btn.closest('.collapse').collapse('hide')
            $('.add-comment').on('click', _addcomment);
            $('.del-comm-ok').on('click', _deletecomment);
            $('.add-reply').on('click', _addreply)
        }
    });
}
function _deletecomic(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    var btn = $(this);
    var cid = btn.attr('data-cid');
    window.location.replace('/eliminar_comic/' + cid + '/');
}
function _setpdfthumb(file,canvas){
    var fileReader = new FileReader();
    fileReader.onload = function () {
        var pdfData = new Uint8Array(this.result);
        // Using DocumentInitParameters object to load binary data.
        var loadingTask = pdfjsLib.getDocument({ data: pdfData });
        loadingTask.promise.then(function (pdf) {
            console.log('PDF loaded');

            // Fetch the first page
            var pageNumber = 1;
            pdf.getPage(pageNumber).then(function (page) {
                console.log('Page loaded');

                var scale = 0.15;
                var viewport = page.getViewport({ scale: scale });

                canvas = canvas[0]
                var context = canvas.getContext('2d');
                canvas.height = viewport.height;
                canvas.width = viewport.width;

                // Render PDF page into canvas context
                var renderContext = {
                    canvasContext: context,
                    viewport: viewport
                };
                var renderTask = page.render(renderContext);
                renderTask.promise.then(function () {
                    console.log('Page rendered');
                });
            });
        }, function (reason) {
            // PDF loading error
            console.error(reason);
        });
    };
    fileReader.readAsArrayBuffer(file);
}
function _cappdf_change(evt) {
    var file = evt.target.files[0];
    var canvas = $(this).closest('.modal-body').find('#pdfViewer').first();
    _setpdfthumb(file,canvas);
}
function _modpdf_shown(evt){
    var mod=$(this);
    var canvas = mod.find('#pdfViewer').first();
    var pdfurl = mod.attr('data-pdf-url')
    var blob = null;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", pdfurl);
    xhr.responseType = "blob";
    xhr.onload = function()
    {
        blob = xhr.response;
        _setpdfthumb(blob,canvas);
    }
    xhr.send();
}
function _modcap(evt) {
    evt.preventDefault();
    var form = $(this);
    var cid = form.attr('data-cid');
    var capid = form.attr('data-capid');
    var formData = new FormData(this);
    var strurl =capid?'/modificar_capitulo_ajax/'+capid+'/': '/crear_capitulo_ajax/' + cid + '/';
    $.ajax({
        url: strurl,
        type: 'POST',
        data: formData,
        success: function (data) {
            var capjson =JSON.parse(data.capjson);
            var caphtml = data.caphtml;
            var capid = capjson[0].pk;
            var cont = $('.cont-cap-'+capid);
            if(!cont||!cont.length){
                $('#addcap').modal('hide');
               setTimeout(function(){$('.cont-caps').append(caphtml);},500);
                
            }else{
                $('#modcap-'+capid).modal('hide');
                setTimeout(function(){cont.replaceWith(caphtml);},500);
                
            }
          
           // alert(data)
        },
        error: function (data) {
            debugger
        },
        cache: false,
        contentType: false,
        processData: false
    });
}
function _togglefav(evt){

    var btn=$(this);
    var cid = btn.attr('data-cid');
    var iscurrfav = btn.hasClass('fic-heart');
    btn.removeClass('fic-heart');
    btn.removeClass('fic-heart-o');
    btn.addClass(iscurrfav?'fic-heart-o':'fic-heart');
    var cmg = parseInt($('.cantmegusta').text());
    cmg+=iscurrfav?-1:1;
    $('.cantmegusta').text(cmg);
    $.ajax({
        url: '/boton_me_gusta/'+cid,
        type: 'GET',
        success: function (result) {

        }
    });
}
function _togglebm(evt){
    var btn=$(this);
    var cid = btn.attr('data-cid');
    var iscurrbm = btn.hasClass('fic-bookmark');
    btn.removeClass('fic-bookmark');
    btn.removeClass('fic-bookmark-o');
    btn.addClass(iscurrbm?'fic-bookmark-o':'fic-bookmark');
    
    $.ajax({
        url: '/boton_seguidores/'+cid,
        type: 'GET',
        success: function (result) {

        }
    });
}