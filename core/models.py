from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone



#Clases Relacionadas con el Usuario
class Pais(models.Model):

    nombre = models.CharField(max_length=30)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name="Pais"
        verbose_name_plural = "Paises"

class Genero(models.Model):

    nombre = models.CharField(max_length=30)

    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name="Genero"
        verbose_name_plural = "Generos"

class Usuario(User):

    pais = models.ForeignKey(Pais, on_delete=models.CASCADE)
    nota = models.CharField(max_length=200, blank=True, null=True)
    imagen_usuario = models.ImageField(upload_to="imagen_usuario/", blank=True, null=True)
    genero = models.ForeignKey(Genero, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.username

    class Meta:
        verbose_name="Usuario"
        verbose_name_plural = "Usuarios"
        permissions = [("autor","autor"),]#crea el permiso autor con el cual filtramos en usuario

#Clases Relacionadas con el Comic


class Comic(models.Model):

    usuario =           models.ForeignKey(Usuario, on_delete=models.CASCADE)
    nombre =            models.CharField(max_length=200)
    desc =              models.TextField()#dejar como maximo 400
    fecha_creacion =    models.DateTimeField(default=timezone.now)
    imagen_comic =      models.ImageField(upload_to="imagen_comic/", blank=True, null=True)
    imagen_banner =     models.ImageField(upload_to="imagen_banner/", blank=True, null=True)
    #seguidores =        models.ManyToManyField(settings.AUTH_USER_MODEL,Usuario)
    #me_gustas =         models.ManyToManyField(settings.AUTH_USER_MODEL,Usuario)


    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name="Comic"
        verbose_name_plural = "Comics"

#---------------------------------------Puede fallar :/
class Me_Gustas(models.Model):#posible tabla de rompiento ._.

    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    comic = models.ForeignKey(Comic, on_delete=models.CASCADE)

    def __str__(self):
        return " %s le gusta a %s." % (self.comic.nombre,self.usuario.username)

    class Meta:
        verbose_name="Me_Gustas"
        verbose_name_plural = "Me_Gustas"

class Seguidores(models.Model):#posible tabla de rompiento ._.

    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    comic = models.ForeignKey(Comic, on_delete=models.CASCADE)

    def __str__(self):
        return " %s es seguido por  %s." % (self.comic.nombre,self.usuario.username)

    class Meta:
        verbose_name="Seguidores"
        verbose_name_plural = "Seguidores"
#---------------------------------------Puede fallar :/

class Capitulo(models.Model):

    comic = models.ForeignKey(Comic, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=200)
    numero_capitulo =  models.CharField(max_length=5)
    vistas = models.IntegerField(default=0)
    fecha_creacion = models.DateTimeField(default=timezone.now)
    pdf = models.FileField(upload_to="capitulo/", blank=True, null=True)

    def __str__(self):
        return "Capitulo %s: %s." % (self.numero_capitulo, self.nombre)

    class Meta:
        verbose_name="Capitulo"
        verbose_name_plural = "Capitulos"

class Tags(models.Model):

    nombre = models.CharField(max_length=200)
    color =models.CharField(max_length=200, blank=True, null=True)
   
    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name="Tags"
        verbose_name_plural = "Tags"

class Comic_tags(models.Model):#posible tabla de rompiento ._. 

    tags = models.ForeignKey(Tags, on_delete=models.CASCADE)
    comic = models.ForeignKey(Comic, on_delete=models.CASCADE)

    def __str__(self):
        return " %s: %s." % (self.tags, self.comic)

class Vistos_Comic(models.Model):#posible tabla de rompiento ._.

    comic = models.ForeignKey(Comic, on_delete=models.CASCADE)
    cant = models.IntegerField(default=0)

    def add_vista(self):
        self.cant = self.cant + 1
        self.save()

    def cant_vistas(self):
        return self.cant

    def __str__(self):
        return " %s tiene %s visualizaciones." % (self.comic, self.cant)

#los putos comentarios
class Comment(models.Model):
    comic = models.ForeignKey(Comic, on_delete=models.CASCADE)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    #comment = models.ForeignKey(Comment, on_delete=models.CASCADE, blank=True, null=True)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    approved_comment = models.BooleanField(default=True)

    def approve(self):
        if self.approved_comment==False:
            self.approved_comment = True
            self.save()
        
    def disapprove(self):
        if self.approved_comment:
            self.approved_comment = False
            self.save()

    def __str__(self):
        return self.text

class SubComment(Comment):
    comment = models.ForeignKey(Comment, on_delete=models.CASCADE, blank=True, null=True,related_name='Subcomment')
    
    def __str__(self):
        return self.text

    class Meta:
        verbose_name="SubComment"
        verbose_name_plural = "SubComment"

class Info_Adicional(models.Model):

    usuario= models.ForeignKey(Usuario,on_delete=models.CASCADE)
    link_twitter = models.CharField(max_length=200, blank=True, null=True)
    link_instagram = models.CharField(max_length=200, blank=True, null=True)
    link_facebook = models.CharField(max_length=200, blank=True, null=True)
    link_linkedin = models.CharField(max_length=200, blank=True, null=True)
    link_pinterest = models.CharField(max_length=200, blank=True, null=True)
    link_tumblr = models.CharField(max_length=200, blank=True, null=True)
    link_youtube = models.CharField(max_length=200, blank=True, null=True)
    link_deviantart = models.CharField(max_length=200, blank=True, null=True)
    link_artstation = models.CharField(max_length=200, blank=True, null=True)
    link_my_web = models.CharField(max_length=200, blank=True, null=True)

    def __str__(self):
        return self.usuario.username

    class Meta:
        verbose_name="Info_Adicional"
        verbose_name_plural = "Info_Adicional"


class Tools(models.Model):
    nombre = models.CharField(max_length=100, blank=True, null=True)
    
    def __str__(self):
        return self.nombre

    class Meta:
        verbose_name="Tools"
        verbose_name_plural = "Tools"

class Me_Tools(models.Model):#posible tabla de rompiento ._.

    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    tools = models.ForeignKey(Tools, on_delete=models.CASCADE)

    def __str__(self):
        return " %s usa %s." % (self.tools.nombre,self.usuario.username)

    class Meta:
        verbose_name="Me_Tools"
        verbose_name_plural = "Me_Tools"

class Slider(models.Model):
    comic = models.ForeignKey(Comic, on_delete=models.CASCADE)

    def __str__(self):
        return self.comic.nombre

    class Meta:
        verbose_name="Slider"
        verbose_name_plural = "Slider"

class Leido(models.Model):#posible tabla de rompiento ._.

    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    capitulo = models.ForeignKey(Capitulo, on_delete=models.CASCADE)

    def __str__(self):
        return " %s visto %s." % (self.capitulo.nombre,self.usuario.username)

    class Meta:
        verbose_name="Leido"
        verbose_name_plural = "Leidos"