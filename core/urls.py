from django.urls import path
#from .views import home,bib_filt_ajax, registro_usuario, ingreso_usuario,crear_comic,comic,crear_capitulo,capitulo,perfil_usuario,boton_me_gusta,boton_seguidores,biblioteca,buscar,autores,modificar_usuario,eliminar_usuario, modificar_comic,eliminar_comic, modificar_capitulo,eliminar_capitulo,eliminar_comentario, info_adicional, login_ajax, agregar_comentario_ajax,eliminar_comentario_ajax,agregar_comentario_resp_ajax
from .views import *
from django.contrib.auth.urls import views as auth_views


from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('', home, name='home'),

    #CRUD USUARIO
    path('registrar_usuario/', registro_usuario, name='registro_usuario'),
    path('registrar_usuario_ajax/', registro_usuario_ajax, name='registro_usuario_ajax'),
    path('modificar_usuario/<id>/', modificar_usuario, name='modificar_usuario'),
    path('eliminar_usuario/<id>/', eliminar_usuario, name='eliminar_usuario'),
    path('info_adicional/<id>/', info_adicional, name='info_adicional'),
    #CRUD Comic
    path('crear_comic/',login_required(crear_comic), name='crear_comic'),
    path('modificar_comic/<id>/', modificar_comic, name='modificar_comic'),
    path('eliminar_comic/<id>/', eliminar_comic, name='eliminar_comic'),
    path('eliminar_comentario/<id>/', eliminar_comentario, name='eliminar_comentario'),

    path('biblioteca/', biblioteca, name='biblioteca'),
    path('comic/<id>/', comic, name='comic'),

    path('buscar/', buscar, name='buscar'),
    path('autores/', autores, name='autores'),
    path('autores_pag_ajax/', autores_pag_ajax, name='autores_pag_ajax'),

    path('crear_capitulo/<id>/', crear_capitulo, name='crear_capitulo'),
    path('crear_capitulo_ajax/<id>/', crear_capitulo_ajax, name='crear_capitulo_ajax'),
    path('capitulo/<id>/', capitulo, name='capitulo'),
    path('modificar_capitulo/<id>/', modificar_capitulo, name='modificar_capitulo'),
    path('modificar_capitulo_ajax/<id>/', modificar_capitulo_ajax, name='modificar_capitulo_ajax'),
    path('eliminar_capitulo/<id>/', eliminar_capitulo, name='eliminar_capitulo'),
    path('ver_capitulo/<id>/', ver_capitulo, name='ver_capitulo'),
    
    path('perfil_usuario/<id>/', perfil_usuario, name='perfil_usuario'),
    path('boton_me_gusta/<id>/', boton_me_gusta, name='boton_me_gusta'),
    path('boton_seguidores/<id>/', boton_seguidores, name='boton_seguidores'),
    path('login_ajax/', login_ajax, name='login_ajax'),
    path('bib_filt_ajax/', bib_filt_ajax, name='bib_filt_ajax'),
    
    path('ingreso_usuario/', auth_views.LoginView.as_view(template_name='core/ingreso_usuario.html'), name='login'),
    path('agregar_comentario_ajax/',agregar_comentario_ajax,name="agregar_comentario_ajax"),
    path('eliminar_comentario_ajax/',eliminar_comentario_ajax,name="eliminar_comentario_ajax"),
    path('agregar_comentario_resp_ajax/',agregar_comentario_resp_ajax,name="agregar_comentario_resp_ajax"),
    
]   