from django.shortcuts import render, redirect
from .models import *

from django.db.models import Count, Q
from django.core.paginator import Paginator,EmptyPage, InvalidPage
from django.contrib.auth.decorators import login_required, permission_required, user_passes_test
from django.contrib import messages
import json
from django.http import HttpResponse
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login
from django.core.exceptions import ObjectDoesNotExist
from django.core import serializers
from django.forms.models import model_to_dict
from django.template.loader import render_to_string
#darle un grupo de autor a los usuarios que logeen
"""
from django.contrib.auth.models import Group
g = Group.objects.get(name='groupname') 
g.user_set.add(your_user)
"""

# Create your views here.
def ordenar_MasMeGustas(query):

    def sortSecond(val):#una funcion que devuelve el segundo valor de una lista de 2 elementos
        return val[1] 

    def anadir_elemento(t,e):
        lista=list(t)
        
        lista.append(e)
        t=tuple(lista)
        return t

    id_megusta = []
    for x in query:
        #agrega a las lista una subLista con 2 parametrso, el primero el id del comic y el segundo la cantidad de megustas
       
        id_megusta.append((x.id,Me_Gustas.objects.filter(comic_id = x.id).count()))

    id_megusta.sort(key = sortSecond, reverse = True)#ordena la lista por el segundo elemento (cantidad de megustas)
    t = []

    condicion = True
    for i in id_megusta:
        for e in i:
            if condicion:
                t.append(e)
                condicion = False
            else:
                condicion= True
    
    from itertools import chain
    my_obj_list = []
    for id in t:
        tmp = Comic.objects.get(id = id)
        my_obj_list.append(tmp)
    #and then 
    resp = Comic.objects.none()
    qs = list(chain(resp, my_obj_list))

    return qs

def ordenar_MasPopular(query):
    # @l_id_vistas, LISTA, tiene una lista con listas adentros que contien el (id.usuario, cantidad de vista total comic)


    l_id_vistas = []

    for q in query:
        comic = Comic.objects.filter(usuario_id=q.id)
        conn = 0
        for c in comic:
            try:
                visto_comic =  Vistos_Comic.objects.get(comic_id = c.id)
                conn = conn + visto_comic.cant
            except Vistos_Comic.DoesNotExist:
                pass
            
        
        l_id_vistas.append((q.id,conn))


    def sortSecond(val):#una funcion que devuelve el segundo valor de una lista de 2 elementos
        return val[1] 

    l_id_vistas.sort(key = sortSecond, reverse = True)#ordena la lista por el segundo elemento (cantidad de megustas)


    t = []

    condicion = True
    for i in l_id_vistas:
        for e in i:
            if condicion:
                t.append(e)
                condicion = False
            else:
                condicion= True
    
    from itertools import chain#ni puta idea
    my_obj_list = []
    for id in t:
        try:
            tmp = Usuario.objects.get(id = id)
            my_obj_list.append(tmp)
        except us:
            pass
        
    #and then 
    resp = Usuario.objects.none()
    qs = list(chain(resp, my_obj_list))

    return qs

def is_autor(query):
    from itertools import chain#ni puta idea
    my_obj_list = []
    
    for q in query:
        if Comic.objects.filter(usuario_id=q.id).exists():
            try:
                tmp = Usuario.objects.get(id = q.id)
                my_obj_list.append(tmp)
            except us:
                pass
        
    #and then 
    resp = Usuario.objects.none()
    qs = list(chain(resp, my_obj_list))

    return qs

def home(request):

    comic = Comic.objects.all()
    autores = is_autor(Usuario.objects.all())
    
    comic_reciente =  Comic.objects.order_by('-fecha_creacion')[:12]#para que me muestre las priemas 12 publicacions
    comci_slider = Slider.objects.all()
    comic_populares =  ordenar_MasMeGustas(comic)[:9]

    autores_popular= ordenar_MasPopular(autores)[:6]

    
    variables = {
        'comic':comic,
        'comci_slider' : comci_slider,
        'comic_reciente': comic_reciente,
        'comic_populares':comic_populares,
        'autores_popular':autores_popular,
    }

    try:

        if request.user.id != None and request.user.username !='admin':
            usuario = Usuario.objects.get(id=request.user.id) 
            variables['usuario'] = usuario

    except ObjectDoesNotExist:
        pass

    return render(request,'core/home.html',variables)

def registro_usuario(request):

    generos = Genero.objects.all()
    paises = Pais.objects.all()


    #las metemos en un arreglo
    variables = {
        'generos' : generos,
        'paises' : paises,
    }
    
    

    if request.POST:
        usuario = Usuario()
        usuario.username = request.POST.get('txtUsuario')
        usuario.first_name = request.POST.get('txtPrimerNombre')
        usuario.last_name = request.POST.get('txtSegundoNombre')
        usuario.set_password(request.POST.get('txtPass'))
        usuario.email = request.POST.get('txtMail')
        usuario.nota = request.POST.get('txtNotas')
        usuario.imagen_usuario =request.FILES.get('imagen')



        pais = Pais()
        pais.id = request.POST.get('cboPais')
        genero = Genero()
        genero.id = request.POST.get('cboGenero')
        usuario.pais = pais 
        usuario.genero = genero

        try:
            usuario.save()
            variables['mensaje'] = 'Se ha guardado correctamente'
            #genera un registro de informacion adicional
            info_adicional= Info_Adicional()
            info_adicional.usuario = usuario
            info_adicional.save()
            print('informacion adicional creada')

        except:
            variables['mensaje'] = 'No se ha podido guardar correctamente'
    #User:

    #user = User.objects.create(username = 'manolito', first_name = 'mauel', last_name = 'guevara', email='ejemplo@mail.com', password='Elias_olate.21')


    return render(request,'core/registro_usuario.html',variables)
    #return redirect('home')

def ingreso_usuario(request):

    return render(request,'core/ingreso_usuario.html')

def crear_comic(request):

    tags = Tags.objects.all()
 
    #las metemos en un arreglo
    variables = {
        'tags' : tags,
    }
    try:
        usuario = Usuario.objects.get(id=request.user.id) 
        variables['usuario'] = usuario
    except ObjectDoesNotExist:
        pass
 
    #formulario de agragar comic
    if request.POST:
        comic = Comic()
        usuario = Usuario.objects.get(id=request.user.id)
        comic.usuario = usuario
        comic.nombre = request.POST.get('txtNombre')
        comic.desc = request.POST.get('txtDescripcion')
        comic.imagen_comic =request.FILES.get('imagenComic')
        comic.imagen_banner =request.FILES.get('imagenBanner')
        
        tags_values = request.POST.getlist('t[]')
        if len(tags_values)>3 or len(tags_values)<1:
            variables['mensaje'] = 'No se ha podido guardar correctamente, tienes que elegir entre 1 a 3 etiquetas'
            return render(request,'core/crear_comic.html',variables)
        try:
            comic.save()
            t_comic = Comic.objects.get(nombre=comic.nombre, usuario=usuario)

            #insertamos los tags
            for t in tags_values:
                tag = Tags.objects.get(id = t)#buscamos el tag
                comic_tags=Comic_tags()
                comic_tags.tags = tag
                comic_tags.comic = t_comic
                comic_tags.save()


            variables['mensaje'] = 'Se ha guardado correctamente'
            return redirect('comic', comic.id) 
        except Exception as e:
            print(str(e))
            variables['mensaje'] = 'No se ha podido guardar correctamente'

    return render(request,'core/crear_comic.html',variables)

def comic(request, id):
    comic = Comic.objects.get(id=id)
    autor = Usuario.objects.get(id=comic.usuario.id)
    capitulos = Capitulo.objects.filter(comic_id=id) #--arreglar despues
    scommlst = SubComment.objects.filter(comic_id=id).values_list('comment_ptr_id',flat=True)
    print(scommlst)
    comment = Comment.objects.filter(comic_id=id).order_by('-created_date').exclude(id__in=scommlst)
    print(comment)
    variables = {
        'comic' : comic,
        'autor' : autor,
        'capitulos':capitulos,
        'comment':comment
    }
    try:
        usuario = Usuario.objects.get(id=request.user.id) 
        variables['usuario'] = usuario
    except ObjectDoesNotExist:
        pass
    #formulario
    """
    if request.POST:

        if request.user.is_authenticated:
            #buscamos Comic
            comic = Comic.objects.get(id=id)
            print(comic)
            #buscamos Usuario
            usuario = Usuario.objects.get(id=request.user.id)
            print(usuario)
            comment = Comment()
            comment.comic = comic
            comment.usuario = usuario
            comment.text=request.POST.get('txtComentario')
            print(comment.text)
            

            #capitulo.numero_capitulo = request.POST.get('txtNumero')
            #capitulo.pdf =request.FILES.get('pdf')
            
            try:
                comment.save()
                #variables['mensaje'] = 'Se ha guardado correctamente'
                variables['alerta'] = 'Comentario Agregado con EXITO'
            except:
                print('comentario no guardado')
                variables['alerta'] = 'ERROR al Comentartar'
        else:
            variables['alerta'] = 'Solo pueden comentar los Usuarios Logeados'
    """
    return render(request,'core/comic.html',variables)

def eliminar_comentario(request, id):
    #id es del comentario
    comm = Comment.objects.get(id = id)

    if comm.usuario.id == request.user.id :
        comm.delete()
    
    return redirect('comic', comm.comic.id) 
    
def crear_capitulo(request,id):

    comic = Comic.objects.get(id=id)
    autor = Usuario.objects.get(id=comic.usuario.id)
    #las metemos en un arreglo
    variables = {
        'comic':comic,
        'autor':autor
    }
    try:
        usuario = Usuario.objects.get(id=request.user.id) 
        variables['usuario'] = usuario
    except ObjectDoesNotExist:
        pass
    #formulario de agragar comic
    if request.POST:
        capitulo = Capitulo()
        
        capitulo.comic = comic
        capitulo.nombre = request.POST.get('txtNombre')
        capitulo.numero_capitulo = request.POST.get('txtNumero')
        capitulo.pdf =request.FILES.get('pdf')

        try:
            capitulo.save()
            variables['mensaje'] = 'Se ha guardado correctamente'
        except:
            variables['mensaje'] = 'No se ha podido guardar correctamente'

    return render(request,'core/agregar_capitulo.html',variables)

def id_cap_anterior(lista,id):
    
    tmp = { 'id' : int(id)}
    index = lista.index(tmp)
    index = index-1

    if (0 <= index) and (index < len(lista)):
        return (lista[index]).get('id')
    else:
        return -1

def id_pos_anterior(lista,id):
    
    tmp = { 'id' : int(id)}
    index = lista.index(tmp)
    index = index+1

    if (0 <= index) and (index < len(lista)):
        return (lista[index]).get('id')
    else:
        return -1

def capitulo(request,id):

    cap = Capitulo.objects.get(id=id)
    capitulos = Capitulo.objects.filter(comic_id=cap.comic.id)
    lista = list(Capitulo.objects.filter(comic_id=cap.comic.id).order_by('numero_capitulo').values('id'))

    variables = {
        'cap'       : cap,
        'capitulos' : capitulos,
    }

    
        
        

    if id_cap_anterior(lista,id) != -1:
        cap_pre = Capitulo.objects.get(id=id_cap_anterior(lista,id))
        variables['cap_pre'] = cap_pre.id
    
    if id_pos_anterior(lista,id) != -1:
        cap_pos = Capitulo.objects.get(id=id_pos_anterior(lista,id))
        variables['cap_pos'] = cap_pos.id



    return render(request,'core/capitulo.html',variables)

def perfil_usuario(request,id):

    autor = Usuario.objects.get(id=id)
    comics= Comic.objects.filter(usuario_id = id)

    info_add = Info_Adicional.objects.get(usuario_id = id)


    megustas = Me_Gustas.objects.filter(usuario_id = id)#
    list_id_megusta = []
    for x in megustas:
        list_id_megusta.append(x.comic.id)
    
    megustas=Comic.objects.filter(id__in = list_id_megusta)

    tools = Tools.objects.all()

    variables = {
        'autor': autor,
        'comics' : comics,  
        'megustas':megustas,#
        'info_add' : info_add,
        'tools':tools,
    }
    try:
        usuario = Usuario.objects.get(id=request.user.id) 
        variables['usuario'] = usuario
    except ObjectDoesNotExist:
        pass
    if request.user.id==autor.id:

        seguidos = Seguidores.objects.filter(usuario_id = id)#
        list_id_seguidores = []
        for x in seguidos:
            list_id_seguidores.append(x.comic.id)
        print(list_id_seguidores)
        seguidos=Comic.objects.filter(id__in = list_id_seguidores)
        print(seguidos)
        variables['seguidos'] = seguidos

    return render(request,'core/perfil_usuario.html',variables)

def boton_me_gusta(request, id):
    
    if request.user.id != None and request.user.username !='admin':

        resp = Me_Gustas.objects.filter(comic_id=id ,usuario_id =request.user.id).exists()
        if resp:
            relacion= Me_Gustas.objects.get(comic_id=id ,usuario_id =request.user.id)
            
            try:
                relacion.delete()
                print('relacion de Megusta eliminada')
            except:
                print('relacion de Megusta NO eliminado')
            
        else:
            #buscamos el comic
            comic = Comic()
            comic = Comic.objects.get(id=id)
            #buscamos al usuario
            usuario = Usuario()
            usuario = Usuario.objects.get(id=request.user.id)

            #creamos la relacion
            relacion=Me_Gustas()
            relacion.usuario = usuario
            relacion.comic = comic

            try:
                relacion.save()
                print('Se ha guardado correctamente la relacion')
            except:
                print('No se ha guardado correctamente la relacion')
    else:
        print('no puedes dar megusta si no estas logeado')
    return redirect('comic',id)

def boton_seguidores(request, id):
    
    if request.user.id != None and request.user.username !='admin':

        resp = Seguidores.objects.filter(comic_id=id ,usuario_id =request.user.id).exists()
        if resp:
            relacion= Seguidores.objects.get(comic_id=id ,usuario_id =request.user.id)
            
            try:
                relacion.delete()
                print('relacion de Seguidores eliminada')
            except:
                print('relacion de Seguidores NO eliminado')
            
        else:
            #buscamos el comic
            comic = Comic()
            comic = Comic.objects.get(id=id)
            #buscamos al usuario
            usuario = Usuario()
            usuario = Usuario.objects.get(id=request.user.id)

            #creamos la relacion
            relacion=Seguidores()
            relacion.usuario = usuario
            relacion.comic = comic

            try:
                relacion.save()
                print('Se ha guardado correctamente la relacion')
            except:
                print('No se ha guardado correctamente la relacion')
    else:
        print('no puedes seguir un comic si no estas logeado')
    return redirect('comic',id)

#lo mas asqueroso que he hecho en mucho tiempo 

def buscar_por_tags(tags_values):
    
    def quitar_repetidos(lista_original):
        #quita los elementos repetidos de una lista
        lista_nueva = []
        for i in lista_original:
            if i not in lista_nueva:
                lista_nueva.append(i)
        return lista_nueva

    try:

        comic_tags = Comic_tags.objects.filter(tags_id__in =tags_values)
        
        if not comic_tags.exists():
            return None

        t = []

        #limpiamos la lista de los elementos repetidos

        for x in comic_tags:
            t.append(x.comic.id)

        print("-----1----")
        print(t)
        t = quitar_repetidos(t)
        print("-----2----")
        print(t)

        comic = Comic.objects.filter(id__in = t)
        return comic
    
    except:
        print('Error al buscar tags')
        return None

def buscar(request):
    
    if request.GET:
        queryset = request.GET.get('txtBuscar')
        if queryset:
            comic = Comic.objects.filter(
            Q(nombre__icontains = queryset)|
            Q(desc__icontains   = queryset)
            ).distinct()

            srchusuario = Usuario.objects.filter(
               Q(username__icontains = queryset)|
               Q(nota__icontains   = queryset) 
            )  

    variables = {
        'comic'   : comic,
        'srchusuario' :srchusuario,
        'srchterm':queryset,
    }
    try:
        usuario = Usuario.objects.get(id=request.user.id) 
        variables['usuario'] = usuario
    except ObjectDoesNotExist:
        pass
    return render(request,'core/resultado_busqueda.html', variables)


def biblioteca(request):
    qlimit = 18
    tags = Tags.objects.all()
    
    comic =  Comic.objects.all()
    comic_reciente =  Comic.objects.all().order_by('-fecha_creacion')[:qlimit]
    comic_popular =ordenar_MasMeGustas(comic)[:qlimit]
    comnicPags=Paginator(comic, qlimit)
    recPags=Paginator(comic_reciente, qlimit)
    popPags=Paginator(comic_popular, qlimit)

    if request.POST:
        tags_values = request.POST.getlist('t[]')
        #remplasamos los comic.all(para mostrar solo los que considan con el filtro)
        comic = buscar_por_tags(tags_values)
        
    variables = {
        'comic':comic,
        'comic_reciente':comic_reciente,
        'comic_popular':comic_popular,
        'tags' : tags,
        'comnicPags':comnicPags,
        'recPags':recPags,
        'popPags':popPags,
    }
    try:
        usuario = Usuario.objects.get(id=request.user.id) 
        variables['usuario'] = usuario
    except ObjectDoesNotExist:
        pass
    return render(request,'core/biblioteca.html', variables)

#lo mas asqueroso que he hecho en mucho tiempo 


def autores(request):
    qlimit = 24
    autores =  is_autor(Usuario.objects.all()[:qlimit])
    autores_reciente =  is_autor(Usuario.objects.all().order_by('-date_joined')[:qlimit])

    autores_popular =ordenar_MasPopular(autores)[:qlimit]
    autPags=Paginator(autores, qlimit)
    recPags=Paginator(autores_reciente, qlimit)
    popPags=Paginator(autores_popular, qlimit)

    variables = {
        'autores' : autores,
        'autores_reciente' : autores_reciente,
        'autores_popular' : autores_popular,
        'autPags':autPags,
        'recPags':recPags,
        'popPags':popPags,
    }
    try:
        usuario = Usuario.objects.get(id=request.user.id) 
        variables['usuario'] = usuario
    except ObjectDoesNotExist:
        pass
    return render(request,'core/autores.html', variables)

def modificar_usuario(request,id):

    usuario = Usuario.objects.get(id=id)
    generos = Genero.objects.all()
    paises = Pais.objects.all()
    info_add = Info_Adicional.objects.get(usuario_id = id)
    tools = Tools.objects.all()
    metools = Me_Tools.objects.filter(usuario_id = id )
    #las metemos en un arreglo
    variables = {
        'generos' : generos,
        'paises' : paises,
        'usuario' : usuario,
        'info_add':info_add,
        'tools':tools,
        'metools':metools,
    }
    
    

    if request.POST:
        #usuario = Usuario()
        usuario.username = request.POST.get('txtUsuario')
        usuario.first_name = request.POST.get('txtPrimerNombre')
        usuario.last_name = request.POST.get('txtSegundoNombre')
        contra=request.POST.get('txtPass')
        if len(contra)!=0:
            usuario.set_password(contra)
        
        usuario.email = request.POST.get('txtMail')
        usuario.nota = request.POST.get('txtNotas')
        
        foto = request.FILES.get('imagen',False)#si no hay farchivo retorna un falso
        
        if foto:
            usuario.imagen_usuario=foto
        

        pais = Pais()
        pais.id = request.POST.get('cboPais')
        genero = Genero()
        genero.id = request.POST.get('cboGenero')
        usuario.pais = pais 
        usuario.genero = genero
        
        try:
            usuario.save()
            variables['mensaje'] = 'se a actualizado el usuario'
        except:
            variables['mensaje'] = 'No se ha podido actualizar el usuario'
        info_adicional,created = Info_Adicional.objects.get_or_create(usuario_id= id,defaults={usuario:usuario}) 
        
        info_adicional.link_twitter = request.POST.get('txtTwitter')
        info_adicional.link_instagram = request.POST.get('txtInstagram')
        info_adicional.link_facebook = request.POST.get('txtFacebook')
        info_adicional.link_linkedin = request.POST.get('txtLinkedin')
        info_adicional.link_pinterest = request.POST.get('txtPinterest')
        info_adicional.link_tumblr = request.POST.get('txtTumblr')
        info_adicional.link_youtube = request.POST.get('txtYoutube')
        info_adicional.link_deviantart = request.POST.get('txtDeviantart')
        info_adicional.link_artstation = request.POST.get('txtArtstation')
        info_adicional.link_my_web = request.POST.get('txtMyWeb')

        try:
            
            info_adicional.save()
        except:
            variables['mensaje'] = 'No se ha podido actualizar el usuario'    
            #rescartamos la lista de heramientas
        tags_values = request.POST.getlist('t[]')
        
        #buscamos el comic 
        usuario = Usuario.objects.get(id=id)
        
        #buscamos las antiguas relaciones para eliminarlas
        my_tools_antiguos = Me_Tools.objects.filter(usuario_id = id)
        
        #las eleminamos

        if len(my_tools_antiguos) != 0:
            for mt_delete in my_tools_antiguos:
                print('dentro del bucle')
                mt_delete.delete()


        #insertamos los tags
        for t in tags_values:
            print(t)
            tools = Tools.objects.get(id = t)#buscamos el tag
            my_tools=Me_Tools()
            my_tools.tools = tools
            my_tools.usuario = usuario
            my_tools.save() 

    return render(request, 'core/modificar_usuario.html', variables )

def eliminar_usuario(request, id):

    if not request.user.id:
        print("NO NO NO")
        return redirect('home') 

    usuario = Usuario.objects.get(id=id)
    if usuario.id == request.user.id:
        try:
            usuario.delete()
            mensaje = "Usuario Eliminado correctamente"
            messages.success(request, mensaje)
        except:
            mensaje = "No se ha podido eliminar este Usuario"
            messages.error(request, mensaje)

    return redirect('home') 

def modificar_comic(request, id):
    #id de comic

    comic =  Comic.objects.get(id=id)
    tags = Tags.objects.all()
 
    #las metemos en un arreglo
    variables = {
        'tags' : tags,
        'comic' : comic,
    }
    try:
        usuario = Usuario.objects.get(id=request.user.id) 
        variables['usuario'] = usuario
    except ObjectDoesNotExist:
        pass
    #formulario de agragar comic
    if request.POST:

        comic.nombre = request.POST.get('txtNombre')
        comic.desc = request.POST.get('txtDescripcion')

        imagen_comic = request.FILES.get('imagenComic',False)#si no hay farchivo retorna un falso        
        if imagen_comic:
            comic.imagen_comic=imagen_comic

        imagen_banner = request.FILES.get('imagenBanner',False)#si no hay farchivo retorna un falso        
        if imagen_banner:
            comic.imagen_banner =imagen_banner

        tags_values = request.POST.getlist('t[]')
        if len(tags_values)>3 or len(tags_values)<1:
            variables['mensaje'] = 'No se ha podido guardar correctamente, tienes que elegir entre 1 a 3 etiquetas'
            return render(request,'core/crear_comic.html',variables)
        try:
            #guardamos el comic
            comic.save()
            
            #buscamos el comic 
            t_comic = Comic.objects.get(id=id)
            
            #buscamos las antiguas relaciones para eliminarlas
            tags_comic_antiguos = Comic_tags.objects.filter(comic_id = id)

            #las eleminamos
            for t_delete in tags_comic_antiguos:
                t_delete.delete()


            #insertamos los tags
            for t in tags_values:
                tag = Tags.objects.get(id = t)#buscamos el tag
                comic_tags=Comic_tags()
                comic_tags.tags = tag
                comic_tags.comic = t_comic
                comic_tags.save()


            variables['mensaje'] = 'Se ha modificado correctamente'
            return redirect('comic', comic.id) 
        except:
            variables['mensaje'] = 'No se ha podido modificar correctamente'

    return render(request,'core/modificar_comic.html',variables)

def eliminar_comic(request, id):

    comic = Comic.objects.get(id=id)

    if comic.usuario.id == request.user.id:
        try:
            comic.delete()
            mensaje = "Comic Eliminado correctamente"
            messages.success(request, mensaje)
        except:
            mensaje = "No se ha podido eliminar este Comic"
            messages.error(request, mensaje)


    return redirect('perfil_usuario', request.user.id) 

def modificar_capitulo(request,id):
    #id es del capitulo
    capitulo = Capitulo.objects.get(id=id)
    comic = Comic.objects.get(id=capitulo.comic.id)
    autor = Usuario.objects.get(id=comic.usuario.id)
    #las metemos en un arreglo
    variables = {
        'capitulo' : capitulo,
        'comic':comic,
        'autor':autor,
    }

    if capitulo.comic.usuario.id != request.user.id:
        print('no tienes permiso para modificar capitulos')
        return redirect('home')
    try:
        usuario = Usuario.objects.get(id=request.user.id) 
        variables['usuario'] = usuario
    except ObjectDoesNotExist:
        pass
    #formulario de agragar comic 
    if request.POST:
        capitulo.comic = comic
        capitulo.nombre = request.POST.get('txtNombre')
        capitulo.numero_capitulo = request.POST.get('txtNumero')

        pdf = request.FILES.get('pdf',False)#si no hay farchivo retorna un falso        
        if pdf:
            capitulo.pdf =pdf

        try:
            capitulo.save()
            variables['mensaje'] = 'Se ha guardado correctamente'
        except:
            variables['mensaje'] = 'No se ha podido guardar correctamente'
 
    return render(request,'core/modificar_capitulo.html',variables)

def eliminar_capitulo(request, id):
    #id es del capitulo
    capitulo = Capitulo.objects.get(id=id)

    if capitulo.comic.usuario.id == request.user.id:
        try:
            capitulo.delete()
            mensaje = "Comic Eliminado correctamente"
            messages.success(request, mensaje)
        except:
            mensaje = "No se ha podido eliminar este Comic"
            messages.error(request, mensaje)

    return redirect('comic', capitulo.comic.id) 

def info_adicional(request, id):
    #id es del usuario
    info_adicional = Info_Adicional.objects.get(usuario_id= id)
    tools = Tools.objects.all()

    variables = {
        'info_adicional' : info_adicional,
        'tools': tools
    }

    if request.POST:
       
        info_adicional.link_twitter = request.POST.get('txtTwitter')
        info_adicional.link_instagram = request.POST.get('txtInstagram')
        info_adicional.link_facebook = request.POST.get('txtFacebook')
        info_adicional.link_linkedin = request.POST.get('txtLinkedin')
        info_adicional.link_pinterest = request.POST.get('txtPinterest')
        info_adicional.link_tumblr = request.POST.get('txtTumblr')
        info_adicional.link_youtube = request.POST.get('txtYoutube')
        info_adicional.link_deviantart = request.POST.get('txtDeviantart')
        info_adicional.link_artstation = request.POST.get('txtArtstation')
        info_adicional.link_my_web = request.POST.get('txtMyWeb')

        try:
            
            info_adicional.save()
            
            #rescartamos la lista de heramientas
            tags_values = request.POST.getlist('t[]')
            
            #buscamos el comic 
            usuario = Usuario.objects.get(id=id)
            
            #buscamos las antiguas relaciones para eliminarlas
            my_tools_antiguos = Me_Tools.objects.filter(usuario_id = id)
            
            #las eleminamos

            if len(my_tools_antiguos) != 0:
                for mt_delete in my_tools_antiguos:
                    print('dentro del bucle')
                    mt_delete.delete()


            #insertamos los tags
            for t in tags_values:
                print(t)
                tools = Tools.objects.get(id = t)#buscamos el tag
                my_tools=Me_Tools()
                my_tools.tools = tools
                my_tools.usuario = usuario
                my_tools.save()







            variables['mensaje'] = 'Se ha guardado correctamente'
            #return redirect('home', info_adicional.usuario.id) 

        except:
            variables['mensaje'] = 'No se ha podido guardar correctamente'
 

    
    return render(request, 'core/info_adicional.html',variables)

def login_ajax(request):                                                                                                                         
    if request.method == 'POST':                                                                                                                                                                                                           
        login_form = AuthenticationForm(request, request.POST)
        response_data = {}                                                                              
        if login_form.is_valid():         
            login(request, login_form.get_user())    
            response_data['result'] = 'ok'
            response_data['message'] = 'You"re logged in' 
        else:
            response_data['result'] = 'failed'
            response_data['message'] = "{}".format(', '.join(login_form.error_messages)) 
            #response_data['message']=json.dumps(login_form.error_messages)

        return HttpResponse(json.dumps(response_data), content_type="application/json")  
def bib_filt_ajax(request):
    qlimit = 18
    tags = Tags.objects.all()
    print(request.body) 
    if request.is_ajax():
        json_data = json.loads(request.body)
        page = 1
        tags = json_data["tags"]
        
        if tags:
            res = buscar_por_tags(tags)
            if res:
                comic=res
            else:
                comic=[]    
        else:
            comic = Comic.objects.all()
        print(comic)
        comnicPags=Paginator(comic, qlimit)
        jpage = json_data["page"]
        if jpage:
            page = int(jpage)
        print(comnicPags)
        #comic = comnicPags.page(page).object_list
        if comnicPags:
            try:
                comicp = comnicPags.page(page)
            except (EmptyPage, InvalidPage):
                pass
            if comicp:
                comic=comicp.object_list
        ctx = {
            'comic' : comic,
            'page': page,
            'tags':tags
        }    
        
        return render(request, 'core/biblioteca_filt.html',ctx)
    return HttpResponse("OK") 
def autores_pag_ajax(request):
    qlimit = 24
    autores =  is_autor(Usuario.objects.all())
    if request.is_ajax():
        json_data = json.loads(request.body)
        page = 1
        
        autPags=Paginator(autores, qlimit)
        jpage = json_data["page"]
        if jpage:
            page = int(jpage)
        print(autPags)
        
        if autPags:
            try:
                autp = autPags.page(page)
            except (EmptyPage, InvalidPage):
                pass
            if autp:
                autores=autp.object_list
        ctx = {
            'autores' : autores,
            'page': page,
            
        }    
        
        return render(request, 'core/autores_page.html',ctx)
    return HttpResponse("OK")   
def agregar_comentario_ajax(request):
    response_data ={}
    if request.user.id == None or request.user.username == 'admin':
        response_data['result'] = 'ok'
        response_data['message'] = 'None'
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    
    if request.is_ajax():
        json_data = json.loads(request.body)
        cid = json_data["cid"]
        txt = json_data["txt"]
        usuario = Usuario.objects.get(id=request.user.id) 
        #comic = Comic()
        comic = Comic.objects.get(id = cid)
        #creamos la relacion
        comment=Comment()
        comment.usuario = usuario
        comment.comic = comic
        comment.text = txt
        try:
            comment.save()
            response_data['result'] = 'ok'
            ctx={
                'comm':comment,
                'usuario':usuario,
                'comic':comic,
            }
            response_data['comment'] =render_to_string( 'core/comentario_render.html',ctx)
            print('Se ha guardado correctamente el comentario')
        except Exception as e:
            print('Error')
            response_data['result'] = 'failed'
            response_data['message'] = str(e)
    return HttpResponse(json.dumps(response_data), content_type="application/json")

def eliminar_comentario_ajax(request):
    response_data ={}
    if request.user.id == None or request.user.username == 'admin':
        response_data['result'] = 'ok'
        response_data['message'] = 'None'
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    if request.is_ajax():
        json_data = json.loads(request.body)
        commid = json_data["commid"]
        print("commid="+commid)
       
        try:
            comm = Comment.objects.get(id = int(commid))
            
            comm.delete()
            response_data['result'] = 'ok'
        except Exception as e:
            print('Error')
            response_data['result'] = 'fail'
            response_data['message'] = str(e)

    return HttpResponse(json.dumps(response_data), content_type="application/json")
def agregar_comentario_resp_ajax(request):
    response_data ={}
    if request.user.id == None or request.user.username == 'admin':
        response_data['result'] = 'ok'
        response_data['message'] = 'None'
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    
    if request.is_ajax():
        json_data = json.loads(request.body)
        print("json_data->"+json.dumps(json_data))
        cid = json_data["cid"]
        commid = json_data["commid"]
        txt = json_data["txt"]
        usuario = Usuario.objects.get(id=request.user.id) 
        commptr = Comment.objects.get(id = commid)
        print(commptr)
        #comic = Comic()
        comic = Comic.objects.get(id = cid)
        print(comic)
        
        instance = SubComment()#.objects.create(comment=comment)
        instance.comment=commptr
        instance.usuario = usuario
        instance.comic = comic
        instance.text = txt
        instance.save()
        try:
            
            
            response_data['result'] = 'ok'
            ctx={
                'comm':commptr,
                'sc':instance,
                'usuario':usuario,
                'comic':comic,
                'modosubcomment':True,
            }
            response_data['comment'] =render_to_string( 'core/comentario_render.html',ctx)
            print('Se ha guardado correctamente el comentario')
        except Exception as e:
            print('Error')
            response_data['result'] = 'failed'
            response_data['message'] = str(e)
    return HttpResponse(json.dumps(response_data), content_type="application/json")    
def crear_capitulo_ajax(request,id):

    comic = Comic.objects.get(id=id)
    #autor = Usuario.objects.get(id=comic.usuario.id)
    
    response_data = {
       
    }
    
    if request.POST:
        capitulo = Capitulo()
        
        capitulo.comic = comic
        capitulo.nombre = request.POST.get('txtNombre')
        capitulo.numero_capitulo = request.POST.get('txtNumero')
        capitulo.pdf =request.FILES.get('pdf')

        try:
            capitulo.save()
            data = serializers.serialize('json', Capitulo.objects.filter(id=capitulo.id))
            response_data['capjson']=data
            ctx={
                'cap':capitulo,
                'comic':comic,
                
            }
            response_data['caphtml'] =render_to_string( 'core/caplistitem.html',ctx)
            response_data['mensaje'] = 'Se ha guardado correctamente'
            response_data['result'] = 'ok'
        except Exception as e:
            print('Error')
            response_data['mensaje'] = 'No se ha podido guardar correctamente'
            response_data['result'] = 'failed'
            response_data['message'] = str(e)

    return HttpResponse(json.dumps(response_data), content_type="application/json")  
def modificar_capitulo_ajax(request,id):
    capitulo = Capitulo.objects.get(id=id)
    comic = Comic.objects.get(id=capitulo.comic.id)
    autor = Usuario.objects.get(id=comic.usuario.id)      
    response_data = {
       
    }
    if request.POST:
        capitulo.comic = comic
        capitulo.nombre = request.POST.get('txtNombre')
        capitulo.numero_capitulo = request.POST.get('txtNumero')

        pdf = request.FILES.get('pdf',False)#si no hay farchivo retorna un falso        
        if pdf:
            capitulo.pdf =pdf

        try:
            capitulo.save()
            data = serializers.serialize('json', Capitulo.objects.filter(id=capitulo.id))
            print(data)
            response_data['capjson']=data
            ctx={
                'cap':capitulo,
                'comic':comic,
                
            }
            response_data['caphtml'] =render_to_string( 'core/caplistitem.html',ctx)
            response_data['mensaje'] = 'Se ha guardado correctamente'
            response_data['result'] = 'ok'
        except Exception as e:
            response_data['mensaje'] = 'No se ha podido guardar correctamente'
            response_data['result'] = 'failed'
            response_data['message'] = str(e)
    return HttpResponse(json.dumps(response_data), content_type="application/json")

def ver_capitulo(request,id):
    capitulo = Capitulo.objects.get(id=id)
    comic = Comic.objects.get(id=capitulo.comic.id)
    capitulos = Capitulo.objects.filter(comic_id=comic.id)
    
    autor = Usuario.objects.get(id=comic.usuario.id)
    variables = {
        'capitulo' : capitulo,
        'capitulos' : capitulos,
        'comic':comic,
        'autor':autor,
    }
    if request.user.id != None and request.user.is_superuser == False :
        try:
            resp = Leido.objects.filter(capitulo_id=id ,usuario_id =request.user.id).exists()
            print(resp,' esta es la respuesta')
            if resp == False:
                
                #buscamos el cap
                capitulo = Capitulo()
                capitulo = Capitulo.objects.get(id=id)
                #buscamos al usuario
                usuario = Usuario()
                usuario = Usuario.objects.get(id=request.user.id)
                print('Entramos')
                #creamos la relacion
                relacion=Leido()
                relacion.usuario = usuario
                relacion.capitulo = capitulo
                relacion.save()
        except ValueError:
            pass
    try:
        usuario = Usuario.objects.get(id=request.user.id) 
        variables['usuario'] = usuario
    except ObjectDoesNotExist:
        pass 
    return render(request,'core/ver_capitulo.html',variables)   

def registro_usuario_ajax(request):
    response_data = {
       
    }
    
    if request.POST:
        usuario = Usuario()
        usuario.username = request.POST.get('txtUsuario')
        usuario.first_name = request.POST.get('txtPrimerNombre')
        usuario.last_name = request.POST.get('txtSegundoNombre')
        usuario.set_password(request.POST.get('txtPass'))
        usuario.email = request.POST.get('txtMail')
        usuario.nota = request.POST.get('txtNotas')
        usuario.imagen_usuario =request.FILES.get('imagen')



        pais = Pais()
        pais.id = request.POST.get('cboPais')
        genero = Genero()
        genero.id = request.POST.get('cboGenero')
        usuario.pais = pais 
        usuario.genero = genero

        try:
            usuario.save()
            response_data['result']='OK'
            response_data['mensaje'] = 'Se ha guardado correctamente'
            #genera un registro de informacion adicional
            info_adicional= Info_Adicional()
            info_adicional.usuario = usuario
            info_adicional.save()
            print('informacion adicional creada')

        except:
            response_data['result']='ERROR'
            response_data['mensaje'] = 'No se ha podido guardar correctamente'
    #User:

    #user = User.objects.create(username = 'manolito', first_name = 'mauel', last_name = 'guevara', email='ejemplo@mail.com', password='Elias_olate.21')


    return HttpResponse(json.dumps(response_data), content_type="application/json")  
    