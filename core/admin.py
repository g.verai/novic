from django.contrib import admin

from .models import Pais, Genero,Usuario, Comic, Capitulo , Tags,Comic_tags, Me_Gustas,Seguidores, Vistos_Comic, Comment, SubComment,Info_Adicional, Tools, Me_Tools, Slider,Leido

# Register your models here.
#clase de configuracion para
#el automovil en el admin de django
class PaisAdmin(admin.ModelAdmin):

    list_display = ('id','nombre',)
    #declaramos una tupla
    #list_display = ('correo_electronico', 'run', 'nombre_completo', 'fecha_nacimiento', 'telefono_contacto', 'region', 'ciudad', 'tipo_vivienda')
    #search_fields = ['run', 'nombre_completo']

    #agregaremos un filtro personalizado en el admin
    #list_filter = ('region','ciudad')
admin.site.register(Pais, PaisAdmin)

class GeneroAdmin(admin.ModelAdmin):
    list_display = ('id','nombre',)
admin.site.register(Genero, GeneroAdmin)

class UsuarioAdmin(admin.ModelAdmin):
    list_display = ('username', 'genero' ,'pais','nota')
admin.site.register(Usuario, UsuarioAdmin)

class ComicAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'usuario' ,'fecha_creacion',)
admin.site.register(Comic, ComicAdmin)

class CapituloAdmin(admin.ModelAdmin):
    list_display = ('id','nombre', 'comic' ,'numero_capitulo','vistas','fecha_creacion',)
admin.site.register(Capitulo, CapituloAdmin)

class TagsAdmin(admin.ModelAdmin):
    list_display = ('id','nombre', 'color',)
admin.site.register(Tags, TagsAdmin)

class Comic_TagsAdmin(admin.ModelAdmin):
    list_display = ('tags','comic', )
admin.site.register(Comic_tags, Comic_TagsAdmin)

class Me_GustasAdmin(admin.ModelAdmin):
    list_display = ('usuario','comic', )
admin.site.register(Me_Gustas, Me_GustasAdmin)

class SeguidoresAdmin(admin.ModelAdmin):
    list_display = ('usuario','comic', )
admin.site.register(Seguidores, SeguidoresAdmin)

class Vistos_ComicAdmin(admin.ModelAdmin):
    list_display = ('comic','cant', )
admin.site.register(Vistos_Comic, Vistos_ComicAdmin)

class CommentAdmin(admin.ModelAdmin):
    list_display = ('id','comic','usuario','created_date','approved_comment', )
admin.site.register(Comment, CommentAdmin)

class SubCommentAdmin(admin.ModelAdmin):
    list_display = ('id','comic','usuario','created_date','approved_comment','comment', )
admin.site.register(SubComment, SubCommentAdmin)
#admin.site.register(Comuna)
class Info_AdicionalAdmin(admin.ModelAdmin):
    list_display = ('id','usuario','link_twitter','link_instagram','link_facebook','link_my_web', )
admin.site.register(Info_Adicional, Info_AdicionalAdmin)

#Tools 
class ToolsAdmin(admin.ModelAdmin):
    list_display = ('id','nombre',)
admin.site.register(Tools, ToolsAdmin)


# Me_Tools
class Me_ToolsAdmin(admin.ModelAdmin):
    list_display = ('usuario','tools', )
admin.site.register(Me_Tools, Me_ToolsAdmin)

class SliderAdmin(admin.ModelAdmin):
    list_display = ('id','comic', )
admin.site.register(Slider, SliderAdmin)

# Me_Tools
class LeidoAdmin(admin.ModelAdmin):
    list_display = ('usuario','capitulo', )
admin.site.register(Leido, LeidoAdmin)